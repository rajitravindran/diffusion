package org.diffusion.ethexplorer.runner;

import org.diffusion.ethexplorer.listener.BlockListenerUsingQueue;
import org.diffusion.ethexplorer.processor.BlockProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DiffusionRunner implements ApplicationRunner {

  @Autowired
  BlockListenerUsingQueue blockListenerUsingQueue;
  @Autowired
  BlockProcessor blockProcessor;

  @Override
  public void run(ApplicationArguments args) throws Exception {

    blockListenerUsingQueue.listen();
    blockProcessor.run();
  }
}

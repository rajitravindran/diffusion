package org.diffusion.ethexplorer.listener;

import org.diffusion.ethexplorer.service.MessageQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.web3j.protocol.Web3j;

@Component
public class BlockListenerUsingQueue {

  MessageQueueService queueService;
  Web3j web3j;


  @Autowired
  public BlockListenerUsingQueue(MessageQueueService queueService, Web3j web3j) {
    this.queueService = queueService;
    this.web3j = web3j;
  }

  public void listen() {

    web3j.blockFlowable(false).subscribe(ethBlock -> {
          queueService.add(ethBlock);
        }, Throwable::printStackTrace);
  }
}

package org.diffusion.ethexplorer;

import org.diffusion.ethexplorer.listener.BlockListenerUsingQueue;
import org.diffusion.ethexplorer.service.MessageQueueService;
import org.diffusion.ethexplorer.utils.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.websocket.WebSocketService;


@SpringBootApplication
public class DiffusionApplication {

  public static void main(String[] args) {

    SpringApplication.run(DiffusionApplication.class, args);
    //blockListenerUsingQueue = null;
  }


//  @Autowired
//  BlockListenerUsingQueue blockListenerUsingQueue;
//  @Autowired
//  BlockProcessor blockProcessor;

  @Bean
  public Web3j getWeb3j() {
    WebSocketService webSocketService = null;
    try {
      webSocketService = new WebSocketService(Constants.INFURA_URL, false);
      webSocketService.connect();
    } catch (Exception e) {
      //TODO Retry
    }
    return Web3j.build(webSocketService);
  }

  @Bean
  public BlockListenerUsingQueue getMessageQueueService(MessageQueueService messageQueueService, Web3j web3j) {
    return new BlockListenerUsingQueue(messageQueueService, web3j);
  }



//  @Bean
//  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//    blockListenerUsingQueue.listen();
//    blockProcessor.run();
//    return args -> {};
//  }

}






package org.diffusion.ethexplorer.pojos;

import java.math.BigInteger;
import java.time.Instant;
import lombok.Builder;

@Builder
public class TxnDetails {

  private String txnHash;
  private String senderAddress;
  private BigInteger wethSwapped;
  private BigInteger usdcSwapped;
  private Instant txnTime;

  @Override
  public String toString() {
    return "txn_hash='" + txnHash + '\'' +
        ", sender_address='" + senderAddress + '\'' +
        ", weth_swapped=" + wethSwapped +
        ", usdc_swapped=" + usdcSwapped +
        ", txn_time=" + txnTime;
  }
}

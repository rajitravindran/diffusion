package org.diffusion.ethexplorer.service;

import java.util.Optional;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.web3j.protocol.core.methods.response.EthBlock;

@Service
public interface MessageQueueService {

  public void add(EthBlock block);

  public Optional<EthBlock> remove();

}

package org.diffusion.ethexplorer.utils;

import java.util.Set;

public class Constants {
  public static Set<String> METHOD_SIGNATURES_FOR_SWAP = Set.of("0x3593564c");
  public static String WETH_CONTRACT_ON_UNISWAP = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
  public static String USDC_CONTRACT_ON_UNISWAP = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48";
  public static String UNISWAP_ROUTER_CONTRACT_ADDRESS = "0x7a250d5630b4cf539739df2c5dacb4c659f2488d";
  public static String INFURA_URL = "wss://mainnet.infura.io/ws/v3/0d3aa093b2c14294ad269add163add1c";
}

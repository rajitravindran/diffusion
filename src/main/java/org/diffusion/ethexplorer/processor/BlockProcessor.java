package org.diffusion.ethexplorer.processor;

import java.math.BigInteger;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.diffusion.ethexplorer.pojos.TxnDetails;
import org.diffusion.ethexplorer.service.MessageQueueService;
import org.diffusion.ethexplorer.utils.ConsoleLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlock.Block;
import org.web3j.protocol.core.methods.response.EthLog.LogResult;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;

@Component
@RequiredArgsConstructor
public class BlockProcessor implements Runnable {

  @Autowired
  MessageQueueService messageQueueService;
  @Autowired
  Web3j web3j;

  @Override
  public void run() {
    while (true) {
      Optional<EthBlock> blockOpt = messageQueueService.remove();
      if (blockOpt.isPresent()) {
        Block newBlock = blockOpt.get().getBlock();
        String contractAddress = "0xef1c6e67703c7bd7107eed8303fbe6ec2554bf6b";
        String swapEventSignature = "0xd78ad95fa46c994b6551d0da85fc275fe613ce37657fb8d5e3d130840159d822";

        EthFilter ethFilter = new EthFilter(DefaultBlockParameter.valueOf(newBlock.getNumber()),
            DefaultBlockParameter.valueOf(newBlock.getNumber()), contractAddress);
        ethFilter.addSingleTopic(swapEventSignature);

        try {
          List<LogResult> logResults = web3j.ethGetLogs(ethFilter).send().getLogs();
          if (logResults != null) {
            for (LogResult logResult : logResults) {
              Log log = (Log) logResult.get();
              String data = log.getData().substring(2);
              BigInteger amtIn0 = new BigInteger(data.substring(0, 64), 16);
              BigInteger amtIn1 = new BigInteger(data.substring(65, 128), 16);
              BigInteger amtOut0 = new BigInteger(data.substring(129, 192), 16);
              BigInteger amtOut1 = new BigInteger(data.substring(193, 256), 16);

              Optional<TransactionReceipt> transactionReceiptOpt = web3j.ethGetTransactionReceipt(
                  log.getTransactionHash()).send().getTransactionReceipt();
              if (transactionReceiptOpt.isPresent()) {
                TransactionReceipt transactionReceipt = transactionReceiptOpt.get();
                Block block = web3j.ethGetBlockByHash(transactionReceipt.getBlockHash(), false)
                    .send().getBlock();
                TxnDetails txnDetails = TxnDetails.builder()
                    .txnHash(transactionReceipt.getTransactionHash())
                    .senderAddress(transactionReceipt.getFrom())
                    .wethSwapped(!BigInteger.ZERO.equals(amtIn0) ? amtIn0 : amtOut1)
                    .usdcSwapped(!BigInteger.ZERO.equals(amtIn1) ? amtIn1 : amtOut0)
                    .txnTime(Instant.ofEpochSecond(block.getTimestamp().longValue()))
                    .build();

                ConsoleLogger.print(txnDetails);
              }
            }
          }
        } catch (Exception e) {
          System.out.println(e);
        }
      }
      try {
        Thread.sleep(1000l);
      } catch (Exception e) {
      }
    }
  }
}

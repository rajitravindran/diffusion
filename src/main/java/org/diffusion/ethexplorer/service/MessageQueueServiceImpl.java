package org.diffusion.ethexplorer.service;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import org.springframework.stereotype.Service;
import org.web3j.protocol.core.methods.response.EthBlock;

@Service
public class MessageQueueServiceImpl implements MessageQueueService {

  private Queue<EthBlock> queue = new LinkedList<>();

  @Override
  public void add(EthBlock block) {
    queue.add(block);
  }

  @Override
  public Optional<EthBlock> remove() {
    if(queue.size() > 0) return Optional.of(queue.remove());
    return Optional.empty();
  }

}
